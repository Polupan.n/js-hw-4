"use strict";

//  Теоретичні питання
//  1. Що таке цикл в програмуванні?
// У програмуванні цикл - це конструкція, яка дозволяє виконувати один і той самий блок коду або набір інструкцій декілька разів. Цикли дозволяють автоматизувати повторювані дії та зменшують кількість коду, необхідного для виконання певного завдання.

//  2. Які види циклів є в JavaScript і які їх ключові слова?
// 1.1. Цикл `for` має 3 необов`язкових вирази:
// - ініціалізація: виконується один раз при початку циклу.
// - мова: визначає умову виконання циклу.
// - ітератор: виконується після кожної ітерації циклу.

for (let i = 1; i <= 3; i++) {
  console.log(i);
}
console.log("Цикл завершено!");
// 1.2. Цикл `while` - цикл що виконує бок коду, доки вираз обчислюється як `true`. якщо блок коду дає `false` цикл не виконується.

let number = 5;
let counter = 1;
while (counter <= number) {
  console.log(counter);
  counter++;
}
console.log("Цикл завершено!");

//  3. Чим відрізняється цикл do while від while?ʼ

// `while` - спочатку оцінює вираз, потім виконує його при умові, якщо вираз `true`.
// цикл `do...while` завжди виконує одну ітерацію перед оцінкою виразу.

//  Практичні завдання
//  1. Запитайте у користувача два числа.
// Перевірте, чи є кожне з введених значень числом.
// Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом.
// Виведіть на екран всі числа від меншого до більшого за допомогою циклу for.

let number1, number2;

while (true) {
  number1 = prompt("Введіть перше число:");
  if (number1 === null || number1 === "" || isNaN(number1)) {
    alert("Будь ласка, введіть коректне число для першого числа.");
    continue;
  }
  break;
}

while (true) {
  number2 = prompt("Введіть друге число:");
  if (number2 === null || number2 === "" || isNaN(number2)) {
    alert("Будь ласка, введіть коректне число для другого числа.");
    continue;
  }
  break;
}

number1 = +number1;
number2 = +number2;

console.log("Перше число:", number1);
console.log("Друге число:", number2);

for (let s = Math.min(number1, number2); s <= Math.max(number1, number2); s++) {
  console.log(s);
}
console.log("Цикл завершено!");

//  2. Напишіть програму, яка запитує в користувача число та перевіряє,
//  чи воно є парним числом. Якщо введене значення не є парним числом,
//  то запитуйте число доки користувач не введе правильне значення.

let userNumber;

while (true) {
  userNumber = prompt(`Введіть парне число`);
  if (userNumber === null) {
    console.log(`Ви відмінили введення. Спробуйте ще раз.`);
  } else if (userNumber === "" || isNaN(userNumber)) {
    console.log(`Введене не коректне значення.`);
  } else {
    let evenNumber = +userNumber;
    if (evenNumber % 2 === 0) {
      console.log(`Введене число є парним.`);
      break;
    } else {
      console.log(`Введене чило не є парним. Спробуйте ще раз.`);
    }
  }
}
